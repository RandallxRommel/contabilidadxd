package pojo;

public class Producto {
    private float PrecioUnitario;
    private int cantidad;
    private int id;
    private String nombre;

    public Producto() {
    }
    

    public Producto(float PrecioUnitario, int cantidad, String nombre) {
        this.PrecioUnitario = PrecioUnitario;
        this.cantidad = cantidad;
        this.nombre = nombre;
    }

    public float getPrecioUnitario() {
        return PrecioUnitario;
    }

    public String getNombre() {
        return nombre;
    }
     
    
  

    

    public void setPrecioUnitario(int PrecioUnitario) {
        this.PrecioUnitario = PrecioUnitario;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Producto{" + "PrecioUnitario=" + PrecioUnitario + ", cantidad=" + cantidad + ", id=" + id + ", nombre=" + nombre + '}';
    }

    
    
    
}
