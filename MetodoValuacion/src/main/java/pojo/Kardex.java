/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

/**
 *
 * @author Randal
 */
public class Kardex {
    private int id;
    private long fecha;
    private int entrada;
    private int salida;
    private String detalle;
    private float valorUnitario;
    private int existencia;
    private float precio;
    private float debe;
    private float haber;
    private float saldo;
    private String productName;

    public Kardex(long fecha) {
        this.fecha = fecha;
        
    }
    
    public Kardex(Producto p,long fecha) {
        this.fecha = fecha;
        this.valorUnitario = p.getPrecioUnitario();
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getFecha() {
        return fecha;
    }

    public void setFecha(long fecha) {
        this.fecha = fecha;
    }

    public int getEntrada() {
        return entrada;
    }

    public void setEntrada(int entrada) {
        this.entrada = entrada;
    }

    public int getSalida() {
        return salida;
    }

    public void setSalida(int salida) {
        this.salida = salida;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public float getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(float valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public int getExistencia() {
        return existencia;
    }

    public void setExistencia(int existencia) {
        this.existencia = existencia;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getDebe() {
        return debe;
    }

    public void setDebe(float debe) {
        this.debe = debe;
    }

    public float getHaber() {
        return haber;
    }

    public void setHaber(float haber) {
        this.haber = haber;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }

    @Override
    public String toString() {
        return " \t " + id + "\t " + fecha + "\t   " + detalle + "\t        " + valorUnitario + "  \t\t    " + entrada + "\t     " + salida + "\t\t     " + existencia + "\t\t  " + precio + "\t\t" + debe + "\t" + haber + "\t" + saldo ;
    }
    
}
