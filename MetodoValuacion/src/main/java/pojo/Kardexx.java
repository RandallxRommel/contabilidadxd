/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

/**
 *
 * @author Randal
 */
public class Kardexx {
    private int kardexId;
    
    private long date;
    private String concepto;
    private int entrada;
    private int salida;
    private int existencia;
    private float precioUnitario;
    private float tipo;
    private float debe;
    private float haber;
    private float saldo;

    public Kardexx() {
    }
    

    public Kardexx(Producto p,long date,String io,int cantidad,float precioUnitario,float debe) {
        this.kardexId = p.getId();
        this.date = date;
        this.precioUnitario=precioUnitario;
        this.existencia = existencia;
        if (io.compareToIgnoreCase("entrada")==0){
          this.concepto = "entrada"; 
          this.entrada = cantidad;
          this.debe=cantidad*precioUnitario;
          this.existencia = cantidad +salida;
        }else{
           this.concepto = "salida";
           this.salida = cantidad;
           this.haber=cantidad*precioUnitario;
        }
        
        
        
        
        
    
    }

    public float getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(float precioUnitario) {
        this.precioUnitario = precioUnitario;
    }
     
    public int getKardexId() {
        return kardexId;
    }

    public void setKardexId(int kardexId) {
        this.kardexId = kardexId;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public int getEntrada() {
        return entrada;
    }

    public void setEntrada(int entrada) {
        this.entrada = entrada;
    }

    public int getSalida() {
        return salida;
    }

    public void setSalida(int salida) {
        this.salida = salida;
    }

    public int getExistencia() {
        return existencia;
    }

    public void setExistencia(int existencia) {
        this.existencia = existencia;
    }

  

    public float getTipo() {
        return tipo;
    }

    public void setTipo(float tipo) {
        this.tipo = tipo;
    }

    public float getDebe() {
        return debe;
    }

    public void setDebe(float debe) {
        this.debe = debe;
    }

    public float getHaber() {
        return haber;
    }

    public void setHaber(float haber) {
        this.haber = haber;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }

    @Override
    public String toString() {
        return "Kardexx{" + "kardexId=" + kardexId + ", date=" + date + ", concepto=" + concepto + ", entrada=" + entrada + ", salida=" + salida + ", existencia=" + existencia + ", precioUnitario=" + precioUnitario + ", tipo=" + tipo + ", debe=" + debe + ", haber=" + haber + ", saldo=" + saldo + '}';
    }
    
    
   
    
}
