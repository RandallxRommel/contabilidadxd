package Implementation;

import Collections.IOCollections;
import com.google.gson.Gson;
import dao.ProductoDaoInterface;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import pojo.Producto;

public class ProductoDaoImpl implements ProductoDaoInterface{
    
    private File fhead;
    private File fdata;
    private RandomAccessFile rafhead;
    private RandomAccessFile rafdata;
    private final int SIZE = 120;
    private Gson gson;
    private IOIndex ioIndex;
 
    
    public ProductoDaoImpl(){
        gson = new Gson();
        ioIndex = new IOIndex();
                
    }
    
    private void open() throws FileNotFoundException, IOException {
        fhead = new File("producto.head");
        fdata = new File("producto.data");

        rafhead = new RandomAccessFile(fhead, "rw");
        rafdata = new RandomAccessFile(fdata, "rw");

        if (rafhead.length() <= 0) {
            rafhead.seek(0);
            rafhead.writeInt(0);
            rafhead.writeInt(0);
        }
    }
    
     private void close() throws IOException {
        if (rafhead != null) {
            rafhead.close();
        }
        if (rafdata != null) {
            rafdata.close();
        }
    }
    
    private Field getField(String fieldName) throws NoSuchFieldException{        
        Field field = Producto.class.getField(fieldName);
        return field;        
    }
    
    public void createIndex(String name) throws NoSuchFieldException, IOException{
        Field fieldName = getField(name);
        ioIndex.createIndex(fieldName);
    }

    @Override
    public Producto find(Comparator<Producto> fun) throws IOException {
        Producto d = null;
        
        return d;
    }
    
    @Override
    public List<Producto> any(Predicate<Producto> fun) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Optional<Producto> get(long id) throws IOException {
        open();        
        rafhead.seek(0);
        int n = rafhead.readInt();
        int k = rafhead.readInt();

        if(id > k || id <= 0){
            return Optional.empty();
        }        
 
        int index = IOCollections.binarySearch(rafhead,(int) id, 0, n-1);
        long posHead = 8 + index * 4;
        rafhead.seek(posHead);
        int code = rafhead.readInt();
        
        if (code != id) {
            return Optional.empty();
        }

        long posData = (id - 1) * SIZE;
        rafdata.seek(posData);

        String jsonProducto = rafdata.readUTF();        
        Producto d = gson.fromJson(jsonProducto, Producto.class);
        
        close();
        return Optional.of(d);
    }
    
     @Override
    public List<Producto> getAll() throws IOException {
        open();
        List<Producto> Producto = new ArrayList<>();
        rafhead.seek(0);
        int n = rafhead.readInt();
        int k = rafhead.readInt();

        for (int i = 0; i < n; i++) {
            long poshead = 8 + 4 * i;
            rafhead.seek(poshead);

            int index = rafhead.readInt();
            long posdata = (index - 1) * SIZE;
            rafdata.seek(posdata);

            String jsonProducto = rafdata.readUTF();            
            Producto d = gson.fromJson(jsonProducto, Producto.class);
            Producto.add(d);
        }

        close();
        return Producto;
    }

    @Override
    public void save(Producto t) throws IOException {
        open();
        rafhead.seek(0);
        int n = rafhead.readInt();
        int k = rafhead.readInt();

        long posdata = k * SIZE;

        t.setId(k + 1);
        String jsonProducto = gson.toJson(t);

        rafdata.seek(posdata);
        rafdata.writeUTF(jsonProducto);

        long poshead = 8 + 4 * n;

        rafhead.seek(0);
        rafhead.writeInt(++n);
        rafhead.writeInt(++k);

        rafhead.seek(poshead);
        rafhead.writeInt(k);
        close();
    }

    @Override
    public boolean delete(Producto t) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(Producto t) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
