package Implementation;

import com.google.gson.Gson;
import dao.KardexDaoInterface;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import pojo.Kardex;
import pojo.Producto;

public class KardexDaoImpl implements KardexDaoInterface{
    private File fhead;
    private File fdata;
    private RandomAccessFile rafhead;
    private RandomAccessFile rafdata;
    private final int SIZE=900;
    private Gson gson;
    private IOIndex ioIndex;
    
    public KardexDaoImpl () {
        gson= new Gson();
        ioIndex = new IOIndex();
    }
    
    private void open() throws FileNotFoundException, IOException {
        fhead = new File("Kardex.head");
        fdata = new File("Kardex.data");
        
        rafhead = new RandomAccessFile(fhead, "rw");
        rafdata = new RandomAccessFile(fdata, "rw");
        
        if (rafhead.length()<= 0) {
            rafhead.seek(0);
            rafhead.writeInt(0);
            rafhead.writeInt(0);
            
        }
    }
    
    private void close() throws IOException {
        if(rafhead != null){
            rafhead.close();
        }
        if(rafdata != null) {
            rafdata.close();
        }
    }
    
    private Field getField(String fieldName) throws NoSuchFieldException{
        Field field = Producto.class.getField(fieldName);
        return field;
    }
    
      public void createIndex(String name) throws NoSuchFieldException, IOException{
        Field fieldName = getField(name);
        ioIndex.createIndex(fieldName);
    }
   
      
    @Override
    public Kardex find(Comparator<Kardex> fun) throws IOException {
        Kardex d = null;
        
        return d;
    }

    @Override
    public List<Kardex> any(Predicate<Kardex> fun) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Optional<Kardex> get(long id) throws IOException {
       open();
       rafhead.seek(0);
       int n = rafhead.readInt();
       int k = rafhead.readInt();
       
       if(id> k || id<=0){
           return Optional.empty();
       }
       
       long posData = (id-1)*SIZE;
       rafdata.seek(posData);
       
       String jsonKardex = rafdata.readUTF();
       Kardex d = gson.fromJson(jsonKardex, Kardex.class);
       
       close();
       return Optional.of(d);
       
    }

    @Override
    public List<Kardex> getAll() throws IOException {
        open();
        List<Kardex> Kardex = new ArrayList<>();
        rafhead.seek(0);
        int n = rafhead.readInt();
        int k = rafhead.readInt();
        
         for (int i = 0; i < n; i++) {
            long poshead = 8 + 4 * i;
            rafhead.seek(poshead);

            int index = rafhead.readInt();
            long posdata = (index - 1) * SIZE;
            rafdata.seek(posdata);
            
            String jsonKardex = rafdata.readUTF();
            Kardex d = gson.fromJson(jsonKardex, Kardex.class);
            Kardex.add(d);
         }
         
         close();
         return Kardex;
    }

    public void save(Kardex t) throws IOException {
        open();
        rafhead.seek(0);
        int n = rafhead.readInt();
        int k = rafhead.readInt();

        long posdata = k * SIZE;
      
        t.setId(k + 1);
        String jsonKardex = gson.toJson(t);
        
        rafdata.seek(posdata);
        rafdata.writeUTF(jsonKardex);
        
        long poshead = 8+4*n;
        
        rafhead.seek(0);
        rafhead.writeInt(++n);
        rafhead.writeInt(++k);

        rafhead.seek(poshead);
        rafhead.writeInt(k);
        close();
    }

    @Override
    public boolean delete(Kardex t) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(Kardex t) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
}
