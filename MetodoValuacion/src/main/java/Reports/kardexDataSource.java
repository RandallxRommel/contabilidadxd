package Reports;

import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import pojo.Kardex;


public class kardexDataSource implements JRDataSource{
    
    private List<Kardex> K = new ArrayList<Kardex>();
    private int index = -1;

    @Override
    public boolean next() throws JRException {
        return ++index<K.size();
    }
    
    public void addKardex(Kardex Kardex){
        this.K.add(Kardex);
    }

    @Override
    public Object getFieldValue(JRField jrf) throws JRException {
        Object valor = null;
        
        if("id".equals(jrf.getName())){
            
            valor = K.get(index).getId();
            
        }
        
        else if ("fecha".equals(jrf.getName())) {
            valor = K.get(index).getFecha();
        }
        else if("detalle".equals(jrf.getName())) {
            valor = K.get(index).getDetalle();
        }
        else if("valor unitario".equals(jrf.getName())) {
            valor = K.get(index).getValorUnitario();
        }
        else if ("Entrada".equals(jrf.getName())) {
            valor = K.get(index).getEntrada();
        }
        else if ("Salida".equals(jrf.getName())) {
            valor = K.get(index).getSalida();
        }
        else if ("Existencia".equals(jrf.getName())){
            valor = K.get(index).getExistencia();
        }
        else if ("Precio".equals(jrf.getName())){
            valor = K.get(index).getPrecio();
        }
        else if ("Debe".equals(jrf.getName())) {
            valor = K.get(index).getDebe();
        }
        else if ("Haber".equals(jrf.getName())) {
            valor = K.get(index).getHaber();
        }
        else if ("Saldo".equals(jrf.getName())) {
            valor = K.get(index).getSaldo();
        }
   
    return valor;
}
}