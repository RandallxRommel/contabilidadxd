/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App;

import Implementation.KardexDaoImpl;
import Implementation.ProductoDaoImpl;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import pojo.Kardex;
import pojo.Producto;

/**
 *
 * @author Randal
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int option = 0;
        ProductoDaoImpl pdao = new ProductoDaoImpl();
        KardexDaoImpl kdao = new KardexDaoImpl();
        List<Producto> products;
        int option2 = 0;
        Scanner entrada = new Scanner(System.in);
        do {
            menu();
            option = entrada.nextInt();
            switch (option) {
                
                case 1:
                    do {
                        
                        System.out.println("1.movimiento de entrada");
                        System.out.println("2.movimiento de salida");
                        System.out.println("3.mostrar Kardex");
                        System.out.println("4.Salir");
                        option2=entrada.nextInt();
                    switch (option2) {
                        case 1:
                            Kardex ka;
                        try {
                            ka = kdao.get(kdao.getAll().size()-1).orElse(null);
                            System.out.println("Ingrese el nombre del producto");
                            String nombre = entrada.next();
                            entrada.nextLine();
                            System.out.println("Ingrese la cantidad ");
                            int cantidad = entrada.nextInt();
                            System.out.println("Ingrese precio unitario");
                            float pUnitario = entrada.nextFloat();
                            Producto np = new Producto(pUnitario,cantidad,nombre);
                            
                            try {
                                pdao.save(np);
                                 System.out.println("Producto guardado satisfactoriamente");
                             } catch (IOException e) {
                                 System.out.println("Error al guardar el producto"+e.toString());
                             }
                            System.out.println("Ingrese la fecha ");
                            long fecha = entrada.nextLong();
                            Kardex k =new Kardex(np,fecha);
                            k.setDetalle("Entrada");
                            System.out.println("Ingrese la cantidad de articulos a ingresar");
                            int in = entrada.nextInt();
                            k.setEntrada(in);
                            if(ka!=null){
                                
                                k.setExistencia(in+ka.getExistencia());
                            }else{
                               k.setExistencia(in);
                            }
                            k.setSalida(0);
                            k.setPrecio(np.getPrecioUnitario());
                            k.setDebe(k.getEntrada()*k.getValorUnitario());
                            
                         
                            
                            try {
                                if(kdao.getAll().isEmpty()||ka==null){
                                    k.setSaldo(k.getDebe());
                                }else{
                                    k.setSaldo(k.getDebe()+ka.getSaldo());
                                }
                            } catch (IOException ex) {
                                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                            }
                           kdao.save(k);
                        } catch (IOException ex) {
                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                        }
                            
                            
                          
                            break;

                        case 2:
                            
                        {
                            try {
                                ka = kdao.get(kdao.getAll().size()).orElse(null);
                                int existenciasP = ka.getExistencia();
                                System.out.println("Ingrese el nombre del producto");
                                String nombre = entrada.next();
                                entrada.nextLine();
                                System.out.println("Ingrese la cantidad ");
                                int cantidad = entrada.nextInt();
                               
                                
                                System.out.println("Ingrese la fecha ");
                                long fecha = entrada.nextLong();
                                Kardex k =new Kardex(fecha);
                                k.setDetalle("Salida");
                                System.out.println("Ingrese la salida");
                                int out = entrada.nextInt();
                                k.setSalida(out);
                                  if(ka!=null||kdao.getAll()!=null){
                                
                                     k.setExistencia(existenciasP-out);
                                }else{
                                         k.setExistencia(out);
                                    }
                                k.setEntrada(0);
                                k.setPrecio(ka.getPrecio());
                                k.setHaber(k.getEntrada()*k.getValorUnitario());
                                k.setSaldo(k.getDebe()-k.getHaber());
                                kdao.save(k);
                                } catch (IOException ex) {
                                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                            break;

                        case 3:
                        {
                            try {
                                System.out.format("\tid \tfec\t\tDetalle \t Valor Unitario\tEntrada\tSalida\t Existencia\t Precio\t Debe\t\tHaber\t\t Saldo \n");
                                List<Kardex> kardex = kdao.getAll();
                                for (Kardex k : kardex) {
                                    System.out.format("\t %d    %d\t\t%s\t\t%.2f",k.getId(),k.getFecha(),k.getDetalle(),k.getValorUnitario());
                                    System.out.println("");
                                    
                                }
                            } catch (IOException ex) {
                                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                          
                            break;
                        case 4:
                            break;

                        default:
                            throw new AssertionError();
                    }
                
                    } while (option2!=4);     
                    
                    
                    break;
                    
                case 2:
                    
                    break;
                case 3:
                    try {
                                System.out.format("\tid \tfec\t\tDetalle \tValor Unitario\tEntrada\tSalida\t Existencia\t Precio \tDebe\t\tHaber\t\t Saldo \n");
                                List<Kardex> kardex = kdao.getAll();
                                for (Kardex k : kardex) {
                                    System.out.format("\t %d    %d\t\t%s\t\t    %.2f \t  %d \t  %d\t     %d\t\t  %.2f\t \t%.2f\t \t %.2f\t\t%.2f",k.getId(),k.getFecha(),k.getDetalle(),k.getValorUnitario(),k.getEntrada(),k.getSalida(),k.getExistencia(),k.getPrecio(),k.getDebe(),k.getHaber(),k.getSaldo());
                                    System.out.println("");
                                    
                                }
                            } catch (IOException ex) {
                                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                            }
                    
                    break;
                case 4:
                    
                    break;
                default:
                    throw new AssertionError();
            }
            
        } while (option!=4);
        
    }
    private static void menu(){
        System.out.println("1.Mostrar metodo lifo");
        System.out.println("2.Mostrar metodo fifo");
        System.out.println("3.Mostrar metodo pmp");
        System.out.println("4.Salir");
    
    
    }
    private static void menu2(){
        System.out.println("1.Agregar movimiento de entrada");
        System.out.println("2.agregar movimiento de salida");
        System.out.println("4.Salir");
    
    
    }
}
