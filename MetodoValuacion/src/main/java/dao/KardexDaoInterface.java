package dao;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import pojo.Kardex;



public interface KardexDaoInterface extends Dao<Kardex> {
    Kardex find (Comparator<Kardex> fun) throws IOException;
    List<Kardex> any (Predicate<Kardex> fun) throws IOException;
}
