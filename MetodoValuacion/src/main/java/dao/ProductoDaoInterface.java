package dao;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import pojo.Producto;

public interface ProductoDaoInterface extends Dao<Producto>{
    Producto find(Comparator<Producto> fun) throws IOException;
    List<Producto> any (Predicate<Producto> fun) throws IOException;
    
}
