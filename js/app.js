document
  .querySelector("#generar-nombre")
  .addEventListener("submit", cargarNombres);

// Llamar ajax e imprimir resultados
function cargarNombres(e) {
  e.preventDefault();

  //Leer las variables
  const origen = document.getElementById("origen");
  const origenSeleccionado = origen.options[origen.selectedIndex].value;

  const genero = document.getElementById("genero");
  const generoSeleccionado = genero.options[genero.selectedIndex].value;

  const cantidad = document.getElementById("numero").value;

  let url = "";
  url += "https://randomuser.me/api/?";
  //Si hay origen agregarlo a la url

  if (origenSeleccionado != "") {
    url += `nat=${origenSeleccionado}&`;
  }
  //Si hay un genero agregarlo a la ur
  if (generoSeleccionado != "") {
    url += `gender=${generoSeleccionado}&`;
  }
  //Si hay una cantidad agregada a la url
  if (cantidad != "") {
    url += `results=${cantidad}&`;
  }

  //Conectar con ajax
  //Iniciar XMLHTTPRequest
  const xhr = new XMLHttpRequest();
  //Datos e impresion del template
  xhr.open("GET", url, true);
  //Datos e impresion del template
  xhr.onload = function () {
    if (this.status === 200) {
      const nombres = JSON.parse(this.responseText);
      let htmlNombres = "<h2>Nombres Generados</h2>";

      htmlNombres += '<ul class"lista">';

      //Imprimir cada nombre
      nombres.map(nombre => {
        htmlNombres = `
            <li>${nombre.name}`;
      });

      htmlNombres += "</ul>";

      document.getElementById("resultado").innerHTML = htmlNombres;
    }
  };
  // enviar el request
  xhr.send();
}
